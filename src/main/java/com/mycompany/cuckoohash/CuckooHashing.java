/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cuckoohash;

/**
 *
 * @author Windows10
 */
public class CuckooHashing {

    private int key;
    private String value;
    static CuckooHashing[] Table1 = new CuckooHashing[20];
    static CuckooHashing[] Table2 = new CuckooHashing[20];

    public CuckooHashing(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public CuckooHashing() {

    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public int hash1(int key) {
        return key % Table1.length;
    }

    public int hash2(int key) {
        return (key / Table1.length) % Table2.length;
    }

    public CuckooHashing get(int key) {
        if (Table1[hash1(key)] != null) {
            if (Table1[hash1(key)].key == key) {
                return Table1[hash1(key)];
            }
        }
        if (Table2[hash2(key)] != null) {
            if (Table2[hash2(key)].key == key) {
                return Table2[hash1(key)];
            }
        }
        return null;
    }

    public CuckooHashing delete(int key) {
        if (Table1[hash1(key)] != null) {
            if (Table1[hash1(key)].key == key) {
                CuckooHashing temp = Table1[hash1(key)];
                Table1[hash1(key)] = null;
                return temp;
            }
        }
        if (Table2[hash2(key)] != null) {
            if (Table2[hash2(key)].key == key) {
                CuckooHashing temp = Table2[hash2(key)];
                Table2[hash2(key)] = null;
                return temp;
            }
        }
        return null;
    }
    
    public void put(int key, String value) {

        if (Table1[hash1(key)] != null) {
            if(Table1[hash1(key)].key == key){
                Table1[hash1(key)].key = key;
                Table1[hash1(key)].value = value;
                return;
            }
        }
        if (Table2[hash2(key)] != null) {
            if(Table2[hash2(key)].key == key){
                Table2[hash2(key)].key = key;
                Table2[hash2(key)].value = value;
                return;
            }
        }
        int i = 0;

        while (true) {
            if (i == 0) { 
                if (Table1[hash1(key)] == null) {
                    Table1[hash1(key)] = new CuckooHashing();
                    Table1[hash1(key)].key = key;
                    Table1[hash1(key)].value = value;
                    return;
                }
            } else { 
                if (Table2[hash2(key)] == null) {
                    Table2[hash2(key)] = new CuckooHashing();
                    Table2[hash2(key)].key = key;
                    Table2[hash2(key)].value = value;
                    return;
                }
            }


            if (i == 0) {
                CuckooHashing temp  = Table1[hash1(key)];
                String tempValue = Table1[hash1(key)].value;
                int tempKey = Table1[hash1(key)].key;
                Table1[hash1(key)].key = key;
                Table1[hash1(key)].value = value;
                key = tempKey;
                value = tempValue;

            } else {
                String tempValue = Table2[hash2(key)].value;
                int tempKey = Table2[hash2(key)].key;
                CuckooHashing temp = Table2[hash2(key)];
                Table2[hash2(key)].key = key;
                Table2[hash2(key)].value = value;
                key = tempKey;
                value = tempValue;
            }
            i = (i + 1) % 2;
        }
    }

}
