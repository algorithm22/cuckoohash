/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cuckoohash;

/**
 *
 * @author Windows10
 */
public class Test {
    public static void main(String[] args) {
        CuckooHashing cuckoo=new CuckooHashing();
        cuckoo.put(1, "Na Khon");
        cuckoo.put(2, "Krabi");
        cuckoo.put(3, "Yala");
        cuckoo.put(4, "Satun");
        cuckoo.put(5, "Narathiwat");
        cuckoo.put(6, "Phatthalung");
        cuckoo.put(7, "Phang nga");
        cuckoo.put(8, "Hat Yai");
        cuckoo.put(9, "Suratthani");
        cuckoo.put(10, "Phuket");
        
        System.out.println(cuckoo.Table1[3].getValue());
        System.out.println(cuckoo.get(1).getValue());
        cuckoo.delete(1);
        System.out.println(cuckoo.get(1));
    }
}
